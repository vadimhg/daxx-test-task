<?php
/**
 * Created by PhpStorm.
 * User: Вадим
 * Date: 26.07.2017
 * Time: 22:36
 */

namespace Scanner;
//use Area;
//use SpaceObject;
use Scanner\BliffoscopeException;
class Bliffoscope
{
    protected
        $_scanArea,
        $_spaceObjects = [];


    public function addSpaceObject($key, \Object\SpaceObject $object){
        $this->_spaceObjects[$key] = $object;
        return $this;
    }

   public function setScanArea(\Map\Area $scanArea){
        $this->_scanArea = $scanArea;
        return $this;
    }

    public function runScan(){
        $foundedObjects = [];
        if($this->_scanArea instanceof \Map\Area) {
            $areaWidth = $this->_scanArea->getWidth();
            $areaHeight = $this->_scanArea->getHeight();
            foreach ($this->_spaceObjects as $key => $object) {
                if ($object instanceof \Object\SpaceObject) {
                    $objectWidth = $object->getWidth();
                    $objectHeight = $object->getHeight();
                    for($h=0; $h<($areaHeight - $objectHeight); $h++) {
                        for ($w = 0; $w < ($areaWidth - $objectWidth); $w++){
                            $objectForCompare =  $this->_scanArea->formObject($w, $h, $objectWidth, $objectHeight);
                            $result = $object->compareWith($objectForCompare);
                            if($result>50) {
                                $foundedObjects[] = ['name'=>$key, 'x1'=>$w, 'y1'=>$h, 'x2'=>$w+$objectWidth, 'y2'=>$h+$objectHeight, 'percent'=>$result];
                            }
                        }
                    }
                }
            }
        }
        return $foundedObjects;
    }


}
