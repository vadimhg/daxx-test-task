<?php
/**
 * Created by PhpStorm.
 * User: Вадим
 * Date: 26.07.2017
 * Time: 22:48
 */
namespace Object;
class SpaceObject
{
    protected
        $_objectStruct;

    public function loadFromFile($path){
        $this->_objectStruct =  preg_split("/\\r\\n|\\r|\\n/",file_get_contents($path));
        return $this;
    }

    public function setObjectStruct($objectStruct){
        $this->_objectStruct =  $objectStruct;
        return $this;
    }

    public function getWidth(){
        return strlen($this->_objectStruct[0]);
    }

    public function getHeight(){
        return count($this->_objectStruct);
    }

    public function getChar($h, $w){
        return $this->_objectStruct[$h][$w];
    }

    public function compareWith(SpaceObject $other){
        $objectWidth = $this->getWidth();
        $objectHeight = $this->getHeight();
        $right = 0;
        $all = 0;
        for($h=0; $h<($objectHeight); $h++) {
            for ($w = 0; $w < ($objectWidth); $w++){
                if($this->getChar($h, $w)=="+"){
                    $all++;
                    if($other->getChar($h, $w)=="+") {
                        $right++;
                    }
                }
            }
        }
        return (ceil($right/($all)*100));
    }
}