<?php
define('APP_PATH',dirname(__FILE__) . DIRECTORY_SEPARATOR);
require APP_PATH."autoload.php";
$area = (new \Map\Area())->loadFromFile(APP_PATH.'data'.DIRECTORY_SEPARATOR.'TestData.blf');
$starship = (new \Object\SpaceObject())->loadFromFile(APP_PATH.'data'.DIRECTORY_SEPARATOR.'Starship.blf');
$slimeTorpedo = (new \Object\SpaceObject())->loadFromFile(APP_PATH.'data'.DIRECTORY_SEPARATOR.'SlimeTorpedo.blf');
$allFoundedObjects = (new Scanner\Bliffoscope())
    ->setScanArea($area)
    ->addSpaceObject('slimeTorpedo', $slimeTorpedo)
    ->addSpaceObject('starship', $starship)
    ->runScan();


echo "THE NEXT OBJECTS FOUND:<br/>";
foreach($allFoundedObjects as $object) {
    echo "NAME: ".$object['name']." left: ".$object['x1'].' - '.$object['x2'].'  TOP: '.$object['y1'].' - '.$object['y2'].'  PERCENT: '.$object['percent'].'<br/>';
}