<?php
/**
 * Created by PhpStorm.
 * User: Вадим
 * Date: 26.07.2017
 * Time: 22:31
 */
namespace Map;
class Area
{
    protected
        $_mapData = [];

    public function loadFromFile($path){
        $this->_mapData =  preg_split("/\\r\\n|\\r|\\n/",file_get_contents($path));
        return $this;
    }

    public function getWidth(){
        return strlen($this->_mapData[0]);
    }

    public function getHeight(){
        return count($this->_mapData);
    }

    public function formObject($w, $h, $objectWidth, $objectHeight){
        return (new \Object\SpaceObject)->setObjectStruct(array_map(
            function($string) use ($w, $objectWidth){
                return substr($string, $w, $objectWidth - $w);
            },
            array_slice($this->_mapData, $h, $objectHeight - $h))
        );
    }
}