<?php
    spl_autoload_register(function ($class_name) {
        $filename = APP_PATH . str_replace('\\', '/', $class_name) . '.php';
        if (file_exists($filename)) {
            require $filename;
        }
    });